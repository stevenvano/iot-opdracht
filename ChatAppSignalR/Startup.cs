﻿using Owin;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(ChatAppSignalR.Startup))]

namespace ChatAppSignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
