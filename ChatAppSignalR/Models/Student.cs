﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChatAppSignalR.Models
{
    public class Student
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement]
        public int StudentID { get; set; }

        [BsonElement]
        public string FirstName { get; set; }

        [BsonElement]
        public string LastName { get; set; }
    }
}
