﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChatAppSignalR.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ChatAppSignalR.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Chat()
        {
            return View();
        }

        private IMongoCollection<Student> collection;

        public HomeController()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase db = client.GetDatabase("FirstDataBase");
            this.collection = db.GetCollection<Student>("Students");
        }

        public ActionResult Index()
        {
            var model = collection.Find
            (FilterDefinition<Student>.Empty).ToList();
            return View(model);
        }

        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insert(Student stud)
        {
            collection.InsertOne(stud);
            ViewBag.Message = "Employee added successfully!";
            return View();
        }
    }
}