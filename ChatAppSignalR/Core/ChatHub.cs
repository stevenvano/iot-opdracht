﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChatAppSignalR.Models;
using Microsoft.AspNet.SignalR;

namespace ChatAppSignalR.Core
{
    public class ChatHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello();
        }

        public void Send(string name, string message, string date)
        {
            Clients.All.addNewMessageToPage(name, message, date);
        }
    }
}